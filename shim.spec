%ifarch aarch64
%global efi_arch aa64
%global bootcsv BOOTAA64.CSV
%global bootefi BOOTAA64.EFI
%endif

%ifarch x86_64
%global efi_arch x64
%global bootcsv BOOTX64.CSV
%global bootefi BOOTX64.EFI
%endif

%global debug_package %{nil}
%global __debug_package 1
%global _binaries_in_noarch_packages_terminate_build 0
%undefine _debuginfo_subpackages

%global efidir %{_vendor}
%global shimdir %{_datadir}/shim/%{version}-%{release}/%{efi_arch}
%global shimefivendor /boot/efi/EFI/%{efi_vendor}/
%global shimBOOT  /boot/efi/EFI/BOOT/

%global enable_sm 0
%global vendor_cert %{SOURCE3}

Name:	   	   shim
Version:	   15.7
Release:	   11
Summary:	   First-stage UEFI bootloader
ExclusiveArch:     x86_64 aarch64
License:	   BSD
URL:	 	   https://github.com/rhboot/shim
Source0:	   https://github.com/rhboot/shim/releases/download/%{version}/shim-%{version}.tar.bz2
Source1:	   BOOTAA64.CSV
Source2:	   BOOTX64.CSV
Source3:	   openEuler_ca.der

Patch1:backport-CVE-2023-40546.patch
Patch2:backport-CVE-2023-40551-pe-relocate-Fix-bounds-check-for-MZ-b.patch
Patch3:backport-pe-relocate-make-read_header-use-checked-arithmetic-.patch
Patch4:backport-Add-primitives-for-overflow-checked-arithmetic-opera.patch
Patch5:backport-CVE-2023-40550-pe-Fix-an-out-of-bound-read-in-verify.patch
Patch6:backport-pe-relocate-Ensure-nothing-else-implements-CVE-2023-40550.patch
Patch7:backport-CVE-2023-40548-Fix-integer-overflow-on-SBAT-section-.patch
Patch8:backport-CVE-2023-40547-avoid-incorrectly-trusting-HTTP-heade.patch
Patch9:backport-Further-mitigations-against-CVE-2023-40546-as-a-clas.patch
Patch10:backport-CVE-2023-40549-Authenticode-verify-that-the-signatur.patch
Patch11:backport-CVE-2023-0464.patch
Patch12:backport-CVE-2023-3446.patch
Patch13:backport-CVE-2023-0465.patch
Patch14:backport-CVE-2023-2650.patch
Patch15:backport-CVE-2024-0727.patch
Patch16:backport-Always-clear-SbatLevel-when-Secure-Boot-is-disabled.patch

# Feature for shim SMx support
Patch9000:Feature-shim-openssl-add-ec-support.patch
Patch9001:Feature-shim-openssl-add-ecdsa-support.patch
Patch9002:Feature-shim-openssl-add-sm2-and-sm3-support.patch
Patch9003:Feature-shim-cryptlib-support-sm2-signature-verify.patch
Patch9004:Feature-shim-support-sm2-and-sm3-algorithm.patch
# Feature for shim TPCM support
Patch9005:Feature-add-tpcm-support-with-ipmi-channel.patch

BuildRequires: elfutils-libelf-devel openssl-devel openssl git pesign gnu-efi gnu-efi-devel gcc vim-common efivar-devel

%ifarch aarch64
BuildRequires: binutils >= 2.37-7
%endif
Requires:      dbxtool efi-filesystem mokutil
Provides:      bundled(openssl) = 1.0.2k
Provides:      shim-%{efi_arch} = %{version}-%{release}
Obsoletes:     shim-%{efi_arch} < %{version}-%{release}

%description
Initial UEFI bootloader that handles chaining to a trusted full \
bootloader under secure boot environments.

%package debuginfo
Summary:	Debug information for shim-unsigned
Requires:	%{name}-debugsource = %{version}-%{release}
AutoReqProv:	0

%description debuginfo
This package provides debug information for package %{expand:%%{name}} \
Debug information is useful when developing applications that \
use this package or when debugging this package.

%package debugsource
Summary:	Debug Source for shim-unsigned
AutoReqProv:	0

%description debugsource
This package provides debug information for package %{expand:%%{name}} \
Debug information is useful when developing applications that \
use this package or when debugging this package.

%prep
#chmod +x %{SOURCE100}
%autosetup -n shim-%{version} -p1 -S git
git config --unset user.email
git config --unset user.name
mkdir build-%{efi_arch}

%build
COMMITID=$(cat commit)
MAKEFLAGS="TOPDIR=.. -f ../Makefile COMMITID=${COMMITID} "
MAKEFLAGS+="EFIDIR=%{efidir} PKGNAME=shim RELEASE=%{release} "
MAKEFLAGS+="ENABLE_HTTPBOOT=true ENABLE_SHIM_HASH=true "
%if 0%{enable_sm}
	MAKEFLAGS+="ENABLE_SHIM_SM=true "
%endif
%if "%{vendor_cert}" != ""
	MAKEFLAGS+="VENDOR_CERT_FILE+=%{vendor_cert} "
%endif
MAKEFLAGS+="%{_smp_mflags}"

cd build-%{efi_arch}
make ${MAKEFLAGS} DEFAULT_LOADER='\\\\grub%{efi_arch}.efi' all
cd ..

%if 0%{?openEuler_sign_rsa}
echo "start sign"
sh /usr/lib/rpm/brp-ebs-sign --efi %{_builddir}/shim-%{version}/build-%{efi_arch}/shim%{efi_arch}.efi || [ $? -eq 2 ] && echo "failed to sign, skip signgture"
sh /usr/lib/rpm/brp-ebs-sign --efi %{_builddir}/shim-%{version}/build-%{efi_arch}/fb%{efi_arch}.efi || [ $? -eq 2 ] && echo "failed to sign, skip signgture"
sh /usr/lib/rpm/brp-ebs-sign --efi %{_builddir}/shim-%{version}/build-%{efi_arch}/mm%{efi_arch}.efi || [ $? -eq 2 ] & echo "failed to sign, skip signgture"
mv %{_builddir}/shim-%{version}/build-%{efi_arch}/shim%{efi_arch}.efi.sig %{_builddir}/shim-%{version}/build-%{efi_arch}/shim%{efi_arch}.efi ||:
mv %{_builddir}/shim-%{version}/build-%{efi_arch}/fb%{efi_arch}.efi.sig %{_builddir}/shim-%{version}/build-%{efi_arch}/fb%{efi_arch}.efi ||:
mv %{_builddir}/shim-%{version}/build-%{efi_arch}/mm%{efi_arch}.efi.sig %{_builddir}/shim-%{version}/build-%{efi_arch}/mm%{efi_arch}.efi ||:
%endif

%install
COMMITID=$(cat commit)
MAKEFLAGS="TOPDIR=.. -f ../Makefile COMMITID=${COMMITID} "
MAKEFLAGS+="EFIDIR=%{efidir} PKGNAME=shim RELEASE=%{release} "
MAKEFLAGS+="ENABLE_HTTPBOOT=true ENABLE_SHIM_HASH=true "

cd build-%{efi_arch}
make ${MAKEFLAGS} \
	DEFAULT_LOADER='\\\\grub%{efi_arch}.efi' \
	DESTDIR=${RPM_BUILD_ROOT} \
	install-debuginfo install-debugsource

install -d -m 0700 ${RPM_BUILD_ROOT}/%{shimBOOT}
install -m 0700 fb%{efi_arch}.efi ${RPM_BUILD_ROOT}/%{shimBOOT}
install -m 0700 mm%{efi_arch}.efi ${RPM_BUILD_ROOT}/%{shimBOOT}
install -m 0700 shim%{efi_arch}.efi ${RPM_BUILD_ROOT}/%{shimBOOT}/%{bootefi}
install -d -m 0700 ${RPM_BUILD_ROOT}/%{shimefivendor}
install -m 0700 *.efi ${RPM_BUILD_ROOT}/%{shimefivendor}
install -m 0700 *.hash ${RPM_BUILD_ROOT}/%{shimefivendor}
%ifarch aarch64
install -m 0700 %{SOURCE1} ${RPM_BUILD_ROOT}/%{shimefivendor}
%endif
%ifarch x86_64
install -m 0700 %{SOURCE2} ${RPM_BUILD_ROOT}/%{shimefivendor}
%endif
%if "%{_vendor}" != "openEuler"
    iconv -f UTF-16LE -t UTF-8 ${RPM_BUILD_ROOT}/%{shimefivendor}/%{bootcsv} > /tmp/%{bootcsv}.tmp
    sed -i -e 's/openeuler/%{_vendor}/g' -e 's/openEuler/%{_vendor}/g' /tmp/%{bootcsv}.tmp
    iconv -f UTF-8 -t UTF-16LE /tmp/%{bootcsv}.tmp > ${RPM_BUILD_ROOT}/%{shimefivendor}/%{bootcsv}
%endif

# install the debug symbols
install -d ${RPM_BUILD_ROOT}/usr/lib/debug/%{shimefivendor}
install -m 644 fb%{efi_arch}.efi.debug ${RPM_BUILD_ROOT}/usr/lib/debug/%{shimefivendor}
install -m 644 mm%{efi_arch}.efi.debug ${RPM_BUILD_ROOT}/usr/lib/debug/%{shimefivendor}
install -m 644 shim%{efi_arch}.efi.debug ${RPM_BUILD_ROOT}/usr/lib/debug/%{shimefivendor}

cd ..

%check
make test

%files
%license COPYRIGHT
%{shimBOOT}/fb%{efi_arch}.efi
%{shimBOOT}/mm%{efi_arch}.efi
%{shimBOOT}/%{bootefi}
%{shimefivendor}/%{bootcsv}
%{shimefivendor}/*.efi
%{shimefivendor}/*.hash

%files debuginfo
%defattr(-,root,root,-)
/usr/lib/debug/*
%exclude /usr/lib/debug/.build-id

%files debugsource
%defattr(-,root,root,-)
%dir /usr/src/debug/%{name}-%{version}-%{release}
/usr/src/debug/%{name}-%{version}-%{release}/*

%changelog
* Wed May 8 2024 lijuzhang <lijuzhang@inspur.com> - 15.7-11
- replace vendor for BOOTX64.CSV or BOOTAA64.CSV

* Tue May 7 2024 jinlun <jinlun@huawei.com> - 15.7-10
- Fix the TPCM feature issue, and ignore signing failures
  due to insufficient permissions.

* Mon Apr 1 2024 jinlun <jinlun@huawei.com> - 15.7-9
- Interface for replacing the EFI signature

* Mon Mar 25 2024 yixiangzhike <yixiangzhike007@163.com> - 15.7-8
- backport patch from upstream

* Wed Feb 28 2024 jinlun <jinlun@huawei.com> - 15.7-7
- add signature for secureboot

* Wed Feb 28 2024 zhengxiaoxiao <zhengxiaoxiao2@huawei.com> - 15.7-6
- fix CVE-2023-3446 CVE-2023-0465 CVE-2023-2650 CVE-2024-0727

* Mon Feb 19 2024 jinlun <jinlun@huawei.com> -15.7-5
- fix CVE-2023-0464

* Mon Feb 5 2024 jinlun <jinlun@huawei.com> - 15.7-4
- add tpcm support with ipmi channel

* Thu Jan 25 2024 jinlun <jinlun@huawei.com> - 15.7-3
- fix CVE-2023-40547 CVE-2023-40548 CVE-2023-40549 CVE-2023-40550
- CVE-2023-40551

* Tue Nov 7 2023 jinlun <jinlun@huawei.com> - 15.7-2
- fix CVE-2023-40546

* Tue Jul 18 2023 jinlun <jinlun@huawei.com> - 15.7-1
- update version to 15.7

* Wed Jul 5 2023 yuxiating <yuxiating@xfusion.com> 15.6-10
- update openssl version description

* Mon Feb 27 2023 fushanqing <fushanqing@kylinos.cn> - 15.6-9
- delete debuginfo and debugsource subpackage buildarch

* Sat Feb 11 2023 jinlun <jinlun@huawei.com> - 15.6-9
- add code check in shim

* Tue Dec 13 2022 jinlun <jinlun@huawei.com> - 15.6-8
- add edition number

* Fri Nov 18 2022 luhuaxin <luhuaxin1@huawei.com> - 15.6-7
- Add some switch for easy to use

* Fri Nov 11 2022 luhuaxin <luhuaxin1@huawei.com> - 15.6-6
- Bugfix for SM2 certificate chain verify

* Fri Nov 11 2022 luhuaxin <luhuaxin1@huawei.com> - 15.6-5
- Bugfix for SMx feature

* Thu Nov 10 2022 jinlun <jinlun@huawei.com> - 15.6-4
- Add BuildRequires on the arrch64

* Tue Nov 8 2022 luhuaxin <luhuaxin1@huawei.com> - 15.6-3
- Optimize patches for SMx feature

* Mon Oct 31 2022 luhuaxin <luhuaxin1@huawei.com> - 15.6-2
- Feature: shim support SM2 and SM3

* Fri Jul 15 2022 Chenxi Mao <chenxi.mao@suse.com> - 15.6-1
- Upgrade version to 15.6 to fix CVE-2022-28737

* Tue Jul 5 2022 Hugel <gengqihu1@h-partners.com> - 15.4-3
- fix shim occasionally crashes in _relocate() on AArch64

* Thu Mar 3 2022 panxiaohe <panxh.life@foxmail.com> - 15.4-2
- list files into debuginfo subpackage

* Tue Dec 21 2021 panxiaohe <panxiaohe@huawei.com> - 15.4-1
- update version to 15.4

* Tue Mar 9 2021 panxiaohe <panxiaohe@huawei.com> - 15-20
- modify efidir to _vendor

* Mon Jun 22 2020 leiju <leiju4@huawei.com> - 15-19
- fix unaligned point value with GCC9

* Tue Mar 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 15-18
- fix wrong information

* Mon Feb 24 2020 openEuler Buildteam <buildteam@openeuler.org> - 15-17
- Remove excess packaged files

* Thu Feb 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 15-16
- add BuildRequires: gcc

* Sun Jan 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 15-15
- List debug files

* Wed Nov 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 15-14
- Remove excess install

* Thu Nov 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 15-13
- Add defination of efi_arch

* Mon Nov 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 15-12
- Add %{bootefi}

* Thu Nov 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 15-11
- Add arch x86_64

* Thu Sep 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 15-10
- Add missing BOOTAA64.CSV

* Thu Sep 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 15-9
- Package init

* Tue Sep 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 15-8
- Package init
